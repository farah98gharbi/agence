/** @format */

import React from "react";
import immobilier from "../img/agent-immobilier.jpg";
const part1 = {
  overflow: "hidden",
  width: "300px",
  borderRadius: " 5px",
  top: "3px",
  position: "relative",
  border: "1px solid #dfdfdf",
  height: "500px",
  flexDirection: "column",
  justifyContent: "space-between",
  padding: "10px",
};
const button={
  //display: inline-flex,
  justifyContent: "center",
  alignItems: "center",
  borderRadius: "40px",
  backgroundColor:" #4169E1",
  width: "300px",
  height: "64px",
  marginTop: "56px",
  color:"white",
  fontStyle: "oblique",
}
const part2={
  width:"290px",
   height:"233px" 
     
}
const part3= {
  fontStyle: "italic",
  fontSize: "13px",
  textAlign: "center",
  paddingTop: "28PX",
  
};

function CardAssets({local , prix}) {
  
  return (
    <div style={part1}>
      <img style={part2} src={immobilier} />
      <div style={part3}>
      <h1> {local}</h1>
      <h1>{prix}</h1>
      </div>
      <button type="button" style={button}>PLUS DETAILLE</button>
     
    </div>
  );
}

export default CardAssets;
