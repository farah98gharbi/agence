/** @format */

import React from "react";
import client from "../img/client.png";

const part1 = {
  overflow: "hidden",
  width: "300px",
  borderRadius: " 5px",
  top: "3px",
  position: "relative",
  border: "1px solid #dfdfdf",
  height: "500px",
  flexDirection: "column",
  justifyContent: "space-between",
  padding: "10px",
};
const part2 = {
  width: "226px",
  height: "181px",
  marginLeft: "40px",
};
const part3 = {
  fontStyle: "italic",
  fontSize: "13px",
  textAlign: "center",
  paddingTop: "28PX",
};

const button = {
  //display: inline-flex,
  justifyContent: "center",
  alignItems: "center",
  borderRadius: "40px",
  backgroundColor: " #4169E1",
  width: "300px",
  height: "64px",
  marginTop: "56px",
  color: "white",
  fontStyle: "oblique",
};

function CardClient({ nom, prenom, tel }) {
  return (
    <div style={part1}>
      <img style={part2} src={client} />

      <div style={part3}>
        <h1>{nom}</h1>
        <h1>{prenom}</h1>
        <h1>{tel}</h1>
      </div>

      <div>
        <button type="button" style={button}>
          ASSETS
        </button>
      </div>
    </div>
  );
}

export default CardClient;
